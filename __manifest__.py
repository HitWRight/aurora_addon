# -*- coding: utf-8 -*-
{
    'name': "aurora-addon",

    'summary': """
    Aurora integration services for Odoo.
    """,

    'description': """
    Allows making orders from Odoo directly into Aurora.
    Allows tracking of clientele.
    """,

    'author': "Ignas Lapėnas",
    'website': "lapenas.dev",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Integration',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['crm', 'contacts', 'mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/templates.xml',
        'views/views.xml',
        'views/partner.xml',
        'views/crm.xml',
        'views/config.xml',
        'views/calendar.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}

from odoo import models, fields, api
import requests

class Wizard(models.TransientModel):
    _name = 'aurora.wizard'
    _description = "Wizard: Quick Registration of Clients"

    url = fields.Char(Required=True)

    def extract(self):
        params = self.env['ir.config_parameter'].sudo()
        parserUrl = params.get_param('aurora.rekvizitai_parser_address')

        print(parserUrl)

        headers = {'content-type': 'application/json'}
        r = requests.post('{}/extractor'.format(parserUrl),
                          headers=headers, json= {'url': self.url});

        print(r.text)
        data = r.json()

        kontakt = self.env['res.partner'].browse(self._context.get('active_id'))
        kontakt.street = data['address']
        kontakt.name = data['name'];
        kontakt.entity_type = data['entity_type']
        kontakt.registration_id = data['registration_id']
        kontakt.mobile = data['mobile_phone']
        kontakt.is_company = True
        kontakt.phone = data['phone']
        kontakt.vat = data['vat_id']
        kontakt.website = data['website']

        print(kontakt.read())
        return {}
#             ceo: String::new(),


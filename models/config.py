from odoo import api, fields, models

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    database_server_address = fields.Char('Ip address of aurora mssql database')
    database_name = fields.Char('Aurora database name')
    database_username = fields.Char('Aurora database username')
    database_password = fields.Char('Aurora database password')
    rekvizitai_parser_address = fields.Char('Rekvizitai parser address')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            database_server_address = self.env['ir.config_parameter'].sudo().get_param('aurora.database_server_address'),
            database_name = self.env['ir.config_parameter'].sudo().get_param('aurora.database_name'),
            database_username = self.env['ir.config_parameter'].sudo().get_param('aurora.database_username'),
            database_password = self.env['ir.config_parameter'].sudo().get_param('aurora.database_password'),
            rekvizitai_parser_address = self.env['ir.config_parameter'].sudo().get_param('aurora.rekvizitai_parser_address')
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()

        param.set_param('aurora.database_server_address', self.database_server_address)
        param.set_param('aurora.database_name', self.database_name)
        param.set_param('aurora.database_username', self.database_username)
        param.set_param('aurora.database_password', self.database_password)
        param.set_param('aurora.rekvizitai_parser_address', self.rekvizitai_parser_address)


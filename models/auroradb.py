import pyodbc
import logging

from . import models

_logger = logging.getLogger(__name__)

class Auroradb:
    def __init__(self, databaseAddress, databaseName, databaseUsername, databasePassword):
        _logger.info('Logging into using {}@{}:{} with pwd {}'.format(
            databaseUsername,
            databaseAddress,
            databaseName,
            databasePassword))

        self._conn = pyodbc.connect('Driver={{ODBC Driver 17 for SQL Server}};Server={};Database={};UID={};PWD={};'.format(
            databaseAddress or 'localhost',
            databaseName or '',
            databaseUsername or '',
            databasePassword or ''
        ))
        _logger.info('Logged into Aurora database')

    def Execute(self, query):
        _logger.warning('Executing non sanitized query: {}'.format(query))
        cursor = self._conn.cursor()
        cursor.execute(query)
        return cursor

    def GetClient(self, code):
        _logger.info('Retrieving client by code: {}'.format(code))
        cursor = self._conn.cursor()
        result = cursor.execute('select c.Id,'
                                ' c.Code,'
                                ' c.Name,'
                                ' ot.Name,'
                                ' cg.Name,'
                                ' c.VatCode,'
                                ' c.Telephone,'
                                ' c.Email,'
                                ' c.Address '
                                ' from cli.Clients c'
                                ' inner join cli.ClientGroups cg on cg.Id = c.ClientGroupId'
                                ' inner join cli.OrganizationTypes ot on ot.Id = c.OrganizationTypeId'
                                ' where c.Code = ?', code)

        client = next(
            iter(
                list(
                    map(lambda x: models.Client(x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8]), result))), None)
        return client

    def GetContact(self, clientId, name, surname):
        _logger.info('Retrieving contact: {} {} {}'.format(clientId, name, surname))
        cursor = self._conn.cursor()
        result = cursor.execute("""
        select Id, Name, Surname, ClientId, Position, Comment from cli.Contacts where ClientId = ? and Name = ? and Surname = ?
        """, clientId, name, surname)
        #def __init__(self, id, name, surname, clientId, position, comment):
        return next(iter(list(map(lambda x: models.Contact(x[0], x[1], x[2], x[3], x[4], x[5]), result))), None)

    def AddClient(self, oClient):
        _logger.info('Adding/Updating client: {}'.format(oClient.registration_id))
        if oClient.entity_type == False:
            return

        # Find Organization Type
        cursor = self._conn.cursor()
        result = cursor.execute('select Id, Name from cli.OrganizationTypes where Name=?', oClient.entity_type)
        orgType = next(iter(list(map(lambda x: models.OrganizationType(x[0],x[1]),result))), None)
        if orgType is None:
            _logger.error('Organization type not found: {}'.format(oClient.entity_type))
            return
        _logger.info('Organization type found: {} {}'.format(orgType.Id, orgType.Name))

        # Find Client Group
        tags = oClient.category_id.read()
        clientGroup = None
        for tag in tags:
            result = cursor.execute('select Id, Name from cli.ClientGroups where Name = ?', tag['name'])
            clientGroup = next(iter(list(map(lambda x: models.ClientGroup(x[0], x[1]), result))), None)
            if clientGroup is not None:
                _logger.info('Client group found: {} {}'.format(clientGroup.Id, clientGroup.Name))
                break

        if clientGroup is None:
            _logger.error('Client group not found: {}', list(map(lambda x: x['name'], tags)))
            raise NameError('No clientGroup selected!')
            return

        cursor.execute("""exec odoo.AddClient @Name = ?, @code = ?, @clientGroupId = ?, @OrganizationTypeId = ?, @VatCode = ?, @telephone = ?,
        @Email = ?, @Address = ?, @comment = ?""",
                                      oClient.name,
                                      oClient.registration_id,
                                      clientGroup.Id,
                                      orgType.Id,
                                      oClient.vat,
                                      oClient.phone or "",
                                      oClient.email_formatted or "",
                                      oClient.street or "",
                                      oClient.comment or "")

        self._conn.commit()


        # _logger.info('Client added with Id: {}'.format())


    def AddContact(self, contact, oldName, oldSurname):
        _logger.info('Adding/Updating contact: {}'.format(contact.name))

        cursor = self._conn.cursor()
        a = contact.name.split()
        result = cursor.execute("""
        exec odoo.AddContact
        @clientCode = ?,
        @oldName = ?,
        @name = ?,
        @oldSurname = ?,
        @surname = ?,
        @position = ?
        """, contact.parent_id.registration_id,
                                oldName,
                                a[0] if a[0:] else '',
                                oldSurname,
                                a[1] if a[1:] else '',
                                contact.function)
        self._conn.commit()

        result = cursor.execute("""select top 1 Id from odoo.IdStack order by Num desc""")
        self.aurora_id = next(iter(list(map(lambda x: x[0], result))), None)
        _logger.info('Added/Updated contact with aurora Id: {}'.format(self.aurora_id))


    def ChangeClientCode(self, oldCode, newCode):
        _logger.warning('Changing client code: {} to {}'.format(oldCode, newCode))
        cursor = self._conn.cursor()
        cursor.execute("""
        Update cli.Clients
        set Code = ?
        where Code = ?""", newCode, oldCode)
        self._conn.commit()


    def GetOrder(self, orderId):
        _logger.info('Retrieving order by order number: {}'.format(orderId))
        cursor = self._conn.cursor()
        result = cursor.execute('select * from aur.Orders where Id = ?', orderId)
        return next(iter(list(map(lambda x: models.Order(), result))),None)

    def AddOrder(self, lead, ldapUsername):
        _logger.info('Creating new order using lead: {}'.format(lead.name))
        splitedName = lead.partner_id.name.split()
        contactName = splitedName[0] if splitedName[0:] else ''
        contactSurname = splitedName[1] if splitedName[1:] else ''
        code = lead.partner_id.parent_id.registration_id
        cursor = self._conn.cursor()
        if lead.aurora_order_id == 0:
            result = cursor.execute("""
            exec odoo.AddOrder
            @clientCode = ?,
            @contactName = ?,
            @contactSurname = ?,
            @opportunityName = '',
            @LDAPUsername = ?
            """, code, contactName, contactSurname, ldapUsername)
            self._conn.commit()
            result = cursor.execute("select top 1 Id from odoo.IdStack order by Num desc")
            lead.aurora_order_id = next(iter(list(map(lambda x: x[0],result))), None)
            _logger.info('Generated order Id: {}'.format(lead.aurora_order_id))
            self._conn.commit()
        else:
            _logger.info('Order already exists for lead: {} it\'s number {}'.format(lead.name, lead.aurora_order_id))
            # cursor.execute("""exec odoo.UnreserveOrder @orderId = ?""", lead.aurora_order_id)
            self._conn.commit()


    def ReserveOrder(self, lead):
        _logger.info('Reserving order for lead: {}'.format(lead.name))
        cursor = self._conn.cursor()
        cursor.execute("""exec odoo.ReserveOrder @orderId = ?""", lead.aurora_order_id)
        self._conn.commit()

    def ConfirmOrder(self, lead):
        _logger.info('Confirming order for lead: {}'.format(lead.name))
        cursor = self._conn.cursor()
        cursor.execute("""exec odoo.ConfirmOrder @orderId = ?""", lead.aurora_order_id)
        self._conn.commit()

    def UnconfirmOrder(self, lead):
        _logger.info('Unconfirming order for lead: {}'.format(lead.name))
        cursor = self._conn.cursor()
        cursor.execute("""exec odoo.UnconfirmOrder @orderId = ?""", lead.aurora_order_id)
        self._conn.commit()


class Contact:
    def __init__(self, id, name, surname, clientId, position, comment):
        self.Id = id
        self.Name = name or ''
        self.Surname = surname or ''
        self.ClientId = clientId or 0
        self.Position = position or ''

    Id = 0
    Name = ''
    Surname = ''
    ClientId = 0
    Position = ''
    Comment = ''
    Email = ''
    Phone = ''

class OrganizationType:
    def __init__(self, id, name):
        self.Id = id
        self.Name = name

    Id = 0
    Name = ''

class ClientGroup:
    def __init__(self, id, name):
        self.Id = id
        self.Name = name
    Id = 0
    Name = ''

class Client:
    def __init__(self, id, code, name, organizationType, clientGroup, vatId, phone, email, address):
        self.Id = id
        self.Name = name or ''
        self.Code = code or ''
        self.ClientGroup = clientGroup or ''
        self.OrganizationType = organizationType or ''
        self.VatId = vatId or ''
        self.Phone = phone or ''
        self.Email = email or ''
        self.Address = address or ''
    Id = 0
    Name = ''
    Code = ''
    ClientGroup = ''
    OrganizationType = ''
    VatId = ''
    Phone = ''
    Email = ''
    Address = ''

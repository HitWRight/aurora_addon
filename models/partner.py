# import numpy
from odoo import models, fields, api
from . import auroradb

class Partner(models.Model):
    _inherit = 'res.partner'

    registration_id = fields.Char(Required=True, domain=[('is_company', '=', True)])
    entity_type = fields.Char(Required=True, domain=[('is_company', '=', True)])
    aurora_id = fields.Integer()
    lead_id = fields.Many2many('crm.lead', string="Leads")

    def write(self, vals):
        if(self.is_company == True):
            oldId = self.registration_id
        else:
            oldId = ''
        oldName = self.name
        new_id = super(Partner, self).write(vals)
        if not (self.is_company == True and oldId == ''):
            self._save_values_in_aurora(oldId, oldName)
        return new_id

    def _save_values_in_aurora(self, oldCode, oldName):
        params = self.env['ir.config_parameter'].sudo()
        db = auroradb.Auroradb(params.get_param('aurora.database_server_address'),
                               params.get_param('aurora.database_name'),
                               params.get_param('aurora.database_username'),
                               params.get_param('aurora.database_password'))

        if len(self.parent_id.read()) == 0:
            print('{} {}'.format(oldCode, self.registration_id))
            if oldCode != False and oldCode != self.registration_id:
                db.ChangeClientCode(oldCode, self.registration_id)
            db.AddClient(self)
        else:
            a=oldName.split()
            db.AddContact(self, a[0] if a[0:] else '', a[1] if a[1:] else '')

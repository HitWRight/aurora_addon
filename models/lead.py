import logging

from odoo import models, fields, api
from . import auroradb

_logger = logging.getLogger(__name__)

class Lead(models.Model):
    _inherit = 'crm.lead'

    aurora_order_id = fields.Integer()
    # partner_id = fields.Many2many()
    lead_partner_id = fields.Many2many('res.partner', string="Partners")

    # @api.model
    # def create(self, vals):
    #     # print(vals)
    #     # print(vals['partner_id'])
    #     result = super(Lead, self).create(vals)
    #     # print(result)
    #     # print(self.partner_id.read())
    #     # self.lead_partner_id = [(4, vals['partner_id'])]

    def write(self, vals):
        new_id = super(Lead, self).write(vals)
        print(self.read())
        self.partner_id.lead_id = [(4, self.id)]
        # print(self.read())
        # print(self.env.user.read())
        self._save_values_in_aurora()
        # super(Lead,self).write(vals)
        return new_id

    def _save_values_in_aurora(self):
        params = self.env['ir.config_parameter'].sudo()
        db = auroradb.Auroradb(params.get_param('aurora.database_server_address'),
                               params.get_param('aurora.database_name'),
                               params.get_param('aurora.database_username'),
                               params.get_param('aurora.database_password'))
        # print(self.read())
        if "Offered" in self.stage_id.name:
            _logger.info("Lead {} set to offered".format(self.name))
            # print(self.env.user.login)
            db.AddOrder(self, self.env.user.login)
        if "Reserved" in self.stage_id.name:
            _logger.info("Lead {} set to Reserved".format(self.name))
            db.UnconfirmOrder(self)
            db.ReserveOrder(self)
        if "Confirmed" in self.stage_id.name:
            _logger.info("Lead {} set to Confirmed".format(self.name))
            db.ConfirmOrder(self)
        # if self.lost_reason is not False:
        #     _logger.info("Lead {} Lost".format(self.name))

    # def _register_new_offer(db):


    #     db



